tests
===========

The script `fake_atmega.py` will pretend to be an atmega. It will connect to a serial tty using 9600 baud.

In order to use it for testing, do the following (in the `test` directory)

Create two virtual serial port and connect them
```
socat -d -d pty,raw,echo=0,link=./fake_atmega pty,raw,echo=0,link=client
```

Start the fake atmega server.
```
./fake_atmega.py
```

and connect to it using e.g. `minicom` or your own client
```
minicom -D ./client -b 9600
```
