#!/usr/bin/env python3

import serial
import time
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", help="Port to connect to", dest="port")
args = parser.parse_args()

serial_cfg = {"dev": args.port,
              "baud": 9600}

if __name__ == "__main__":
    print("Running port {}".format(serial_cfg['dev']))

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
        try:
            print("led1on")
            ser.write("led1on\n".encode())
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

            print("zzzz")
            time.sleep(1)

            print("led1off")
            ser.write("led1off\n".encode())
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

            print("zzzz")
            time.sleep(1)

            print("getbtn1")
            ser.write("getbtn1\n".encode())
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

            print("zzzz")
            time.sleep(1)

            print("getadcval")
            ser.write("getadcval\n".encode())
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

        except KeyboardInterrupt:
            print('interrupted!')
