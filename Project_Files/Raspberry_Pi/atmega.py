#!/usr/bin/env python3

import serial


def read_led(ser):
    ser.write("led1val\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply


def read_temp(ser):
    ser.write("getadcval\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply


def read_button(ser):
    ser.write("getbtn1\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply


def led1on(ser):
    ser.write("led1on\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply


def led1off(ser):
    ser.write("led1off\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def raiseError(ser, error):
    ser.write("error=" + str(error) + "\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def getError(ser):
    ser.write("geterror\n".encode())
    reply = ser.readline().decode()   # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def connect(port, baudrate):
    return serial.Serial(port, baudrate, timeout=1)
