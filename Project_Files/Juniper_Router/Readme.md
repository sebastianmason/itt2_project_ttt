set system root-authentication encrypted-password "$1$G22Z2oyY$kSGLHjzVrnUDdqzXlUkGj."; ## SECRET-DATA

set system login class readonly-fa permissions [ view view-configuration ]
set system login user anthony class super-user
set system login user anthony authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyR2eA0d44ZfjrvqpWBn1DnO4160dU9nRj8wZ2OHSByBVYwHM8WfMxYW7hIVGkxhsFwX6TYuoL5E/B9kIRxIZotIT+Q+Z6Q5xDDVVdvkecjBG6huz+UtCxtieggetW/sCjeLXFNxRe7CRZifd3aOyTr03zOS+p6leSphMh+ejX2YJew+HpajST8juc0dgbvK8RPsj9RLUWslCnqADayBDSmVgY1rIcQJBGKOziu1kcX3anrcuaIJLOk2iIpJJNQxFvE7zEk1PeCDOpaDSwU8uPbOb7GNKHFZajZOIz+ZRRMYSBq0KzEhjPpyBSPJJ5TA3LU79Bz9ggWs7y1taQ83MS5NhzX5L7XgByfYh50mY2pcs2mUZlI28iZllG2bgtb1glxK3WSQuKTjD884q2xCSDpTXnZwyzSFH52R4cOlTfH4uxMMQxX2oxACjRDVAoYCJCrjhFJg6tgOKyLLVII+7u7R5ip2WHRMIi6gl3VnJ30h3eQhKs0hd+tw8BqvC2bN10OY0lSM98Z52FkcmapaWVeZIbTlNf/OANPcyBC1SHyErs946tuWCuE6mXo17FarLlu9owQlZoo90Q1NvpWFpwYaAqY0irVECdf2pH8QK85VmgoWrCNZDuKycH+3+11RZQtez5o6g8NmaEkn32hr5xCm84WhZ3F9bMKIRu1zzUuw== aj.peak.08@gmail.com"; ## SECRET-DATA
set system login user jesper class super-user
set system login user jesper authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDHcXH6Z0TZCkhQIlTas7vO/Yiqt9vh7o1b0I8wO8QYevhafrdtkeXE5XCLOogSqvjqL6kTuaUujvK5gPUoZCuh+bwMnrCbpfn5fAJnjk38X4rc7NN6OYHbgdl/K4viE9kKSNxWOnk0eL3INxTZMDA7GrciW8swzz18Sx80QqJXuF/zv46swDE990401ETNdb3gA+MNlFDCd1wi+WZhrrU/rKPwLWb293fy6fZN8pM0OOiOwTMGjfPyu3CBQT1XsRkWbcHnWUWFAfPpvQlqH90QT1Ig+Sl9Dsp1JnCb6WaprAgm71YAr5iVwo44nvfjlsVUN+Pe9wunbtLXUy4m0/47reVCz7PwgWdH4wQJzz3wqIy5MLxI8m9Emlw+1hWz0Aske47DnBg+W3JF0RVDTu6jxUj+F5dc7g8WP2J/4ByDFBknQPCJPeQ1DJnMdajBXaqS8PptHT+48hPjoeq0iXVgq2uUMrxM4tAnSV+JqhlyICQDxfPw2EUk4s/aiT1OFw1fYoaYt/X0NTyClpKKGSNxbOsSVUQxzeXmKKVwnyga+9pp4ZLddbFcMzw/dsAL2s1xkizWfg33jnAM/Dz5rlzZ0YRYxvDCLGTO+jFkIAQWLDL2iWDH5uht5qAsPZKnWTVwwceE+pActZ2J54NKSMgQHMhEmjWwG4GByZ8GoJaACQ== jespertinusaggerholm@gmail.com"; ## SECRET-DATA
set system login user jesper authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCduTPPSzWHvc6yKUFJQmG2SxCtNFF+q9tve/ANfI55Gh75H93phKgObenN4VVnXc5Gtcjh02Wr74KuUXcGbqlrw/P2Dz+9OAeTDVn/oD5VsSyuINcZWoAd+oIZMhpGq3L1ZMVi6DdwC+vbTtBmSbyDWuefq4fKJNrwLZhCkQbpPagdqZjdKq1e1b171N/a513DcaxNWN7WuVpYRwaZPGiJTkdDFYvV2yQD3XEW9StFuek540lnr+xgKvqlqL4B0488Yec9hHbFppO4S2GK0rPk9QLj3n06znf8jFfvw2ebX/Z8GFhaw+iikTQXlf/uSVp1WT4m5rs8Fhcz/MB0N7IN hidde@DESKTOP-GIO1IAT"; ## SECRET-DATA
set system login user kasper class super-user
set system login user kasper authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDe2ldWzIqlsPAasixcvv4s1mfwaEsIyTre+JsMBh1I2k2F//jetEBZ2I/U8//a4mA4bAXU+3G2+2DWsDWRRirguiAkwE18MQgFuv69nBiOV36miscjwJ0h4yWAjP2Bk6CqfO6OBh6cD98bRDIGAz/GR1eWMk1/ohnv4p8cB0tlXpbJLDxcawMOTwHQ+tHfP5sZD9uS0qYsmMfWMp6THgxK0Zj7yu+UcRL4Wf4xZY3E8sr1meegZEq2nZq6XsI907TMFfTciouclDUir9UDAGYwukq7+dYwp1C1X478byH58IXoYsG++3IceqQAqWSHkh7I0hzXyc4I/iblVM7t6MB6AtSPCiqTZSuvcXchHBYKioYgaRUP2aW/Z+fW9w/mCytAeukNIudkL3BEmE2+H6oBy0Q9mxmjI9ngAQitsz+vCG/8aJseEP4f6RJNBJI+0xMMgfq2bqL/E1sBCp/GIWNLIB2VVoo7lx+7kLYYRDRTCpHBhKuzfAJMxQQkJnnArKDJZb76gfEFGQRdlsv6DSe7atwotk1HdNl/5dE9TluoDhBFF4l40AkrF12+Vuu3r8eq3wFbn9zDCJqQbEPF1ngYU1PyfsoEG3UcHtI/VnT+Btezw1uEk/WTW47v4ya8rZSQ4mZIIg+FxHdQP3LXCdU0Sw0sX7icvL20smN+h/QrAQ== Laptop"; ## SECRET-DATA
set system login user laurynas class super-user
set system login user laurynas authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDnUWPsJwjGZxIHxx9X3XjmiRiIHScyTkLTVkCucdxB8RfkDrcDAPBxouoy2xlplOzrm7o2ZRw6kBVrFIOdMEsAg96iVS9Kr1M5lqIUW5XbpIp8MS7DAIUoup/giHZ4qaKsq8g3riMLRT0rTXcfQkJKiD8I+PDhxylbnZmySN4QKWpauOYr6D9SuYrcs+eeaggfSW4Ygw1xDuors3DtWIz/G0tdpGe2PXlqVywpji3c4Uelo6BYsy0bAEUvfQZsPWprPJm5bF7XHX2fX9cXchvCYEk3u/eYDv7GtJbhDQrPrwCgBctOwsT4SDmPebOTYJB745cysQu4GdF4vb/JP5TF laury@MSI"; ## SECRET-DATA
set system login user morten class super-user
set system login user morten authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPoHX1OCuUw1PWMjB9DHVUbtVeWInrCrFXzBpBTGrhU/4Ja7X0Biqqf2nWy60OGBAKce1yjDFco4K1UR5FaUReAflV/72PJaV98PZejb6NTn6A5WGnivSQ7+DrKOh2CSeDsW5ZaodiwTMA1F9eb3wHloSUhshkH2mzgF46Tbys7IqOYnZRnzwWSvUctuXMWYBr8SuY7/tPXTgvUneMA7BjB52oxqyITrT9UcjnhChM5hjBdnqvSe3ZA3Pixo184gQ1PuuUWQZPFOLeJd4VHBLH6ba+cKnNSzFc0qo4NiP2RTDUdEEVSarA/KLIw+7Uc19bqwkiZ9Nhbv/x8P5t3+3d moz@monlennart"; ## SECRET-DATA
set system login user niclas class super-user
set system login user niclas authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHAuTruNyhfJ4VDBqcwIagV+K1/MzSA2SGSU0JGpHuOdKuDaDkQ473gYrfDQAjzNTCaA9LP22WiSOUcztTpXTxpjcaNLMJECrYeXXTIfk45TZQmW/JhMi2eYmrc2IPVKDmKQ3i8liyi7m/OaLUSVB6umVj+V7s73BVTbGVDLrhiFX46dsmVM2wSR1k87RXRkBXq9d4stLt6WDpeGHdFNrz5r5Pj/NB2E09K9l6/eKGiVLpgLgGT6z1cFJDh7kJdPJQZWkhtVuWNdOsbWSisRsGee1nl5KwYfsALmqDwhFbIAqgqLhTz/ii9lls5uBVdfTCB66+zsenkLLoBMY3KS7P Niclas Andersen@DESKTOP-ANAJCBA"; ## SECRET-DATA
set system login user niclas authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCi7DQ5U3mLBuaoVDdO9eCQUbqMTZ4UtMyuChOdLVg1vr8MnNO1ma1lQs+q0Q7EF9s3qkYiB49RPw+x5UQ2HmJMk7XXMCdJiw2/qBwHg+LIKwoM9kXhEinF9U+rw9XyP7lWmMT1KG8g3/77tscDesBB97piT3R1JIner49hsHnRZGZ7/CylYUUny5kIoRsQ+87DSxXFUdIy7Fgh4eaiGWx0v72MOYcG20K0JraC0Pcz/3XnnxwJqZnJFYQpPskJVPaqLFxgL3hUZCikUvckp7cXngirNqidjGU/Xd3oVnDrCiDJt5nRY9ASxPZX9RqKOzWe+ZMt6/DBKYiRLUOTV0f9 Niclas@Niclas_MSI"; ## SECRET-DATA
set system login user sebski class super-user
set system login user sebski authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDWHn5KYVBuWi3PMwqSTBQjICEYnif5Jkt5lvz+TL18hZu6qlGg9HS6EXNSCILz5cLo92lXB8mgeu5Rd82Q0ciVzADoNA/2scY73Ysxt9sRK2hnluklgFixECKdzfl48p7+wMZHrZuI1ysizn3m7xXFJ3UJj5R+fia8Jgdumi5fG50CGUJFJFjtirAH1WBFXFJaN/smREttwACJ5DUo1LPGH20rRCQn3HinclxkBQ8rx2IXJj/vsXGL4wEPzHXci1blUoMWfN7zf9dkOksFVcf4Nmx8lf5Hyrx9jFawclz1Pwjy+DZ7nKT2nwarPzRaxsKjRhZtZm3lsSpkiYlR8AJz38EmbKQ4uylsYfuL+NrX7ufL6soeRgT/OEqQgPEFq+Myvuugfyb4WM4fneE9S0bpja0nQrCI77+u2W6Tl9LMOaNgsmbVi5OfSkUBlS0JY5ulwq3mmJHGcsdA+eWrv9FFWtIpVb9tVaMpSEOuzxURu8ojWx6V+4Sp8k5LxW+oJHluu+qzwf8FE8esaCEdGO1vZ+1u11IDc3IuNw90TWpLckZdBRWaP/hakxTcfGWNA+on/eKwuNWH3zgo+rY73Kqu1qJsomdDDuTiJ/fS47LqxtXbeyj2lj5RXlbF92yEkvPm/An/9QJvtIjbihFpjdOl9NcattYjjVXg2fMAtisBTQ== sebthomle@gmail.com"; ## SECRET-DATA
set system login user thomas class super-user
set system login user thomas authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDASZiLf17rBzE5jkilPjneyiu6oGYRvyBRLeu8Koxx2kq1/K0coq6HbXXVrLMDnPzPy2CLpe0ypHDss4HiyRZosu+eq7DzLYfM9yoeLk9qKGin9UBHQCH1mvPpclpymkmSIi4joyXGcIFdqPrPTDlSCz28cKxr5mOxzlk69BGFtpPvSdwQWxcHfidKTD4KJ+EvwXtQkbE2ZKn7q6OplfIV4l111yj7xpC3sv2AkT3ofE4tiFWbC86fHjrrDkShTkmZflHmUbZuYaD9VxJery+9kp9FYhX+Wik2LoAGNBz/K9Vw6BDzQRHjoLGSq2XA2jloi4I6B/q5oVJ7mp+3Ai5P6pmTNpLQq4REwjBGjx873ewX4mryfyVS0J9aah/1t/2W2ehvqjiqnaYYbVcKloyQdvt5AtcEyr3OKHDczbIgbc7mtJR3axJNSXSRe48NNUjVhoY+SuafK5JCy3gusdVa8OwcbyUmPaeHRF0l+vCiaS8umS6YjE9DVtPoavCgBtrEMzQeJ0cfMc5G4F4PFXjOT+WfZrA0bBO0z5CsjksLq7ZvOKe+nPqESFH62/8vRpnMKIwnXEzm+iM2IWu2CZPcDNPm3Q8qiCJ0RPXvYj5cpSQzr8rNR5DDUVb9Qi6WTzxpxx8mpxJfR9gQywi7v/t2PcW2ct5M6rFyvELYmLjyxQ== ttbargisen@gmail.com"; ## SECRET-DATA
set system login user tihamer class super-user
set system login user tihamer authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCwk0uZM3kv2a2ObYdK1q71nS2ESVYGZF/cjvujMRpD+QfymiS6h9wKRVJi56ZWmGgnPbSFduAJJBtK+aT1V/CIlfLIK1m212EDaGi3WyUxuHMdJlHvcF1wj5n22CtFae30lX5uQ9aDXzwpQmGFukvDrFrhTc57jzUiH0T/bj46GZ/CWhkprSGNCTbUmmG4dO+pd7GQOl1eN6rcUrVzzSR5d5/uAR0G4R1WgHf/PNjbCubJ7ICjplI7P8/tZoPF7Ua32sRKSFhrL5xzD33830BDU/eeNk2dvkD+woCLBes/WOFWZWBeGqrXcakv0Iydp+fq4N9n70K31dYuzemSBWU1DIhgSpBgyg/OinyPonzFd9uUFNsWdToO8lPR7nJsmudwEMxAQfzL9G0/Kbazw8ndkJdQ9uAXBNh7ZgJUazniEOIaxtFNnEJfhS8mwo2WPFKMlkviFAzVQNyt1h7ts8sjIC0tahQ2cosSeF7VQGGOFwhHxEnlWBKakHwBitndIm/Y9tjbX8CqZdB50SWTWVqMVTG19Np4MA1z8jmTD+LS5sZXIKIZ3wpR+GkpAqCqlx0+Z+c7Uqp2xQkCcKmcVDRKJTfk89vFr5TK5EAGzbtnjJeKG5G84Gfn77NgknsK669gMSqQl/65J71skndToNvyFjwGXPzPGXUTNSi6zug+1w== tiha0006@edu.eal.dk"; ## SECRET-DATA


set system services ssh root-login deny-password;


set system services dhcp pool 192.168.30.0/24 address-range low 192.168.30.50 high 192.168.30.100
set system services dhcp pool 192.168.30.0/24 name-server 8.8.8.8
set system services dhcp pool 192.168.30.0/24 router 192.168.30.1
set system services dhcp pool 192.168.20.0/24 address-range low 192.168.20.15 high 192.168.20.254
set system services dhcp pool 192.168.20.0/24 name-server 8.8.8.8
set system services dhcp pool 192.168.20.0/24 router 192.168.20.1
set system services dhcp pool 192.168.10.0/24 address-range low 192.168.10.50 high 192.168.10.100
set system services dhcp pool 192.168.10.0/24 name-server 1.1.1.1
set system services dhcp pool 192.168.10.0/24 router 192.168.10.1



set interfaces ge-0/0/1 unit 0 family ethernet-switching vlan members vlan-10
set interfaces ge-0/0/2 unit 0 family ethernet-switching vlan members vlan-10
set interfaces ge-0/0/3 unit 0 family ethernet-switching vlan members vlan-10
set interfaces ge-0/0/4 unit 0 family ethernet-switching vlan members vlan-10
set interfaces ge-0/0/5 unit 0 family ethernet-switching vlan members vlan-10

set interfaces ge-0/0/6 unit 0 family ethernet-switching vlan members vlan-20
set interfaces ge-0/0/7 unit 0 family ethernet-switching vlan members vlan-20
set interfaces ge-0/0/8 unit 0 family ethernet-switching vlan members vlan-20
set interfaces ge-0/0/9 unit 0 family ethernet-switching vlan members vlan-20
set interfaces ge-0/0/10 unit 0 family ethernet-switching vlan members vlan-20

set interfaces ge-0/0/11 unit 0 family ethernet-switching vlan members vlan-30
set interfaces ge-0/0/12 unit 0 family ethernet-switching vlan members vlan-30
set interfaces ge-0/0/13 unit 0 family ethernet-switching vlan members vlan-30
set interfaces ge-0/0/14 unit 0 family ethernet-switching vlan members vlan-30

set interfaces ge-0/0/15 unit 0 family inet address 10.217.19.211/22

set interfaces vlan unit 10 family inet address 192.168.10.1/24
set interfaces vlan unit 20 family inet address 192.168.20.1/24
set interfaces vlan unit 30 family inet address 192.168.30.1/24

set routing-options static route 0.0.0.0/0 next-hop 10.217.16.1


set security address-book global address pi 192.168.30.10/32        
set security address-book global address rpi-webserver-ext 10.217.19.211/32        
set security address-book global address rpi-webserver-int 192.168.20.8/32        
set security address-book global address rpi-ext-20 10.217.19.211/32        


set security nat destination pool des-nat-pool-vlan-30 address 192.168.30.10/32 port 22
set security nat destination pool des-nat-pool-vlan-10 address 192.168.10.25/32 port 22
set security nat destination pool http-grp-30 address 192.168.30.10/32 port 80


set security nat destination rule-set rs1-vlan-30 from interface ge-0/0/15.0
set security nat destination rule-set rs1-vlan-30 rule r1 match destination-address 10.217.19.211/32
set security nat destination rule-set rs1-vlan-30 rule r1 match destination-port 1300
set security nat destination rule-set rs1-vlan-30 rule r1 then destination-nat pool des-nat-pool-vlan-30

set security nat destination rule-set rs1-vlan-30 rule r2 match destination-address 10.217.19.211/32
set security nat destination rule-set rs1-vlan-30 rule r2 match destination-port 1100
set security nat destination rule-set rs1-vlan-30 rule r2 then destination-nat pool des-nat-pool-vlan-10

set security nat destination rule-set rs1-vlan-30 rule http-grp-30 match destination-address 10.217.19.211/32
set security nat destination rule-set rs1-vlan-30 rule http-grp-30 match destination-port 3000
set security nat destination rule-set rs1-vlan-30 rule http-grp-30 then destination-nat pool http-grp-30





set security policies from-zone trust-20 to-zone trust-20 policy trust-to-trust match source-address any
set security policies from-zone trust-20 to-zone trust-20 policy trust-to-trust match destination-address any
set security policies from-zone trust-20 to-zone trust-20 policy trust-to-trust match application any
set security policies from-zone trust-20 to-zone trust-20 policy trust-to-trust then permit

set security policies from-zone trust-20 to-zone untrust-20 policy trust-to-untrust match source-address any
set security policies from-zone trust-20 to-zone untrust-20 policy trust-to-untrust match destination-address any
set security policies from-zone trust-20 to-zone untrust-20 policy trust-to-untrust match application any
set security policies from-zone trust-20 to-zone untrust-20 policy trust-to-untrust then permit

set security policies from-zone untrust-20 to-zone trust-20 policy web-server-access match source-address any
set security policies from-zone untrust-20 to-zone trust-20 policy web-server-access match destination-address rpi-ext-20
set security policies from-zone untrust-20 to-zone trust-20 policy web-server-access match application any
set security policies from-zone untrust-20 to-zone trust-20 policy web-server-access then permit
set security policies from-zone untrust-20 to-zone trust-20 policy default-deny match source-address any
set security policies from-zone untrust-20 to-zone trust-20 policy default-deny match destination-address any
set security policies from-zone untrust-20 to-zone trust-20 policy default-deny match application any
set security policies from-zone untrust-20 to-zone trust-20 policy default-deny then deny


set security policies from-zone untrust to-zone trust policy untrust-to-trust match source-address any
set security policies from-zone untrust to-zone trust policy untrust-to-trust match destination-address any
set security policies from-zone untrust to-zone trust policy untrust-to-trust match application any
set security policies from-zone untrust to-zone trust policy untrust-to-trust then permit;


set security zones security-zone trust interfaces ge-0/0/1.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/2.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/3.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/4.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/5.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/6.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/7.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/8.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/9.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/10.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/11.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/12.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/13.0 host-inbound-traffic system-services all
set security zones security-zone trust interfaces ge-0/0/14.0 host-inbound-traffic system-services all

set security zones security-zone trust interfaces vlan.30 host-inbound-traffic system-services all
set security zones security-zone trust interfaces vlan.30 host-inbound-traffic protocols all
set security zones security-zone trust interfaces vlan.10 host-inbound-traffic system-services all
set security zones security-zone trust interfaces vlan.10 host-inbound-traffic protocols all


set security zones security-zone untrust interfaces ge-0/0/15.0 host-inbound-traffic system-services all



set security zones security-zone untrust-20 interfaces ge-0/0/10.0 host-inbound-traffic system-services http
set security zones security-zone untrust-20 interfaces ge-0/0/10.0 host-inbound-traffic system-services https



set vlans vlan-10 vlan-id 10
set vlans vlan-10 l3-interface vlan.10
set vlans vlan-20 vlan-id 20
set vlans vlan-20 l3-interface vlan.20
set vlans vlan-30 vlan-id 30
set vlans vlan-30 l3-interface vlan.30s


set security zones security-zone interfaces; 

set security zones security-zone trust-20 host-inbound-traffic system-services all
set security zones security-zone trust-20 host-inbound-traffic protocols all
set security zones security-zone trust-20 interfaces ge-0/0/6.0 host-inbound-traffic system-services all
set security zones security-zone trust-20 interfaces ge-0/0/7.0 host-inbound-traffic system-services all
set security zones security-zone trust-20 interfaces ge-0/0/8.0 host-inbound-traffic system-services all
set security zones security-zone trust-20 interfaces ge-0/0/9.0 host-inbound-traffic system-services all
set security zones security-zone trust-20 interfaces vlan.20 host-inbound-traffic system-services all
set security zones security-zone trust-20 interfaces vlan.20 host-inbound-traffic protocols all

