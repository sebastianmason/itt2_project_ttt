---
title: 'ITT2_project_TTT'
subtitle: 'Project Plan'
authors: ['Thomas Bargisen thom8723@edu.eal.dk', 'Tihamer Biliboc tiha0006@edu.eal.dk', 'Sebastian Mason seba7286@edu.eal.dk']
date: \today
left-header: \today
right-header: Project Plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose

The main goal of this project is to create a small system in which sensors are able to interact, collect and send data to a webserver through a pre-determined network layout.
Among features, we were left with some lee-way to choose in which way these/this sensor is able to be used to solve a problem.

Our proposed issue is in regards to greenhouses and adjusting temperature to get the best growth out of your plants.
Greenhouses are often regulated by opening and closing windows, letting in cold air and letting out hot air.
This causes the humidity to rise and fall, keeping the plants at their optimal temperature, which we can easily regulate using a Temperature Sensitive Sensor and a Motor.

We plan on attaching this sensor to the inside of a greenhouse as well as the motor to the window. A pre-determined humidity or temperature will then cause the window to open, or close.

Optionally, a button may be added to force close or open the window, in case of emergencies and testing.

Additionally, we plan on sending information such as timestamps of opening/closing and the humidity and temperature inside the greenhouse to a webserver through the pre-determined layout, resulting in a logging of actions so that the user can adjust to their liking.

---------------
The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated will evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi.
* Raspberry Pi: Minimal Linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Documentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and GitLab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the Easter holidays.

See the lecture plan for details.


# Organization
## Group Members
- Sebastian Mason
- Tihamer Biliboc
- Thomas Bargisen

## Advisors / Overseers
- Nikolaj Simonsen
- Morten Nielsen

## External resources
### Knowledge
- Classmates
- Teachers

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear. This could be done in relation to the goals of the project.]

TBD: This section must be completed by the students.


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute,


# Risk assessment

Risk assessment is a list of possible risks to the project, and suggestions on how to solve them.

## Project risks

- Teamwork
- Lack of documentation
- Poor project management
- Lack of knowledge
- Lack of motivation
- Electrical failures
- Sick leave

## How to overcome these issues

### Teamwork
- Meetings
- Peer pressure
- Talk to advisor
- Kick out


### Documentation

- Lack of teacher docs
- Lack of student docs
- Who does what in the project, take notes! so you can identify the issues.

### Project Management

- Make sure to keep group meetings, a regular thing. So all are on the same page!
- Never leave issues unresolved! make priorities

### Knowledge

- Seek knowledge / help from class mates.
- Group pressure
- Be honest with the goals, never hide your progress!
- Reduce scope of the tasks/project




### Motivation.

- If you lack the motivation, talk with the team. find a solution!

### Electrical Failures
- Make backups
- Spare parts
- Triple check everything
- Don't make high risk decisions

### Sick Leave
- Keep contact
- Redistribute workflow
- Try to work from home


# Stakeholders
The stakeholders include everyone that is actively involved in the project

## Group members
Actively working on tasks to complete the project 
- Sebastian Mathias Thomle Mason
- Tihamer Biliboc
- Thomas Tøttrup Bargisen

## Project coordinators
Actively overseeing the project 
- Nikolaj Simonsen
- Morten Nielsen

## Customers
Actively interested in the development, financing and use of the product
### Private
- Neighbors
- Small greenhouse owners
### Companies
- Agricultural industry


# Communication

## Group members
All communication within the group will take place via Discord, Messenger or other messaging applications


## Project coordinators
Communication to and from the project coordinators will manifest mainly via weekly meetings, itslearning and e-mails

## Customers
Communication to and from the customers will manifest mainly via e-mail, phone or in person
### Private
- Neighbors
- Small greenhouse owners
### Companies
- Agricultural industry


# Perspectives

This project will serve as a template for other similar project, like the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

## All tests succeed
- read temperature
- send temperature to server
- open / close window from button press
- open / close window from server
- open / close window based on temperature from sensor

## Customer satisfaction
- Feedback from customers

## All project milestones on GitLab are completed




# References

None at this time